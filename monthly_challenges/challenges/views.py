from django.shortcuts import render
from django.http import Http404, HttpResponseNotFound, HttpResponseRedirect
from django.urls import reverse


# Create your views here.

reto_meses_dict = {
    "Enero": "Viajar a Argentina",
    "Febrero": "Visitar la costa Argentina con Pamela",
    "Marzo": "Volver de la costa a Buenos Aires",
    "Abril": "Empezar a trabajar asi sea en negro",
    "Mayo": "Recibir primer sueldo trabajando en negro",
    "Junio": "Celebrar mi cumple",
    "Julio": "Empezar nuevo trabajo en Blanco",
    "Agosto": "Recibir primer pago del trabajo en blanco",
    "Septiembre": "Empezar a ahorrar para traer a samu",
    "Octubre": "Comprar un disfraz para Halloween",
    "Noviembre": "Mandar dinero para colegio nuevo de samu",
    "Diciembre": None,
}



def Index(request):
    meses = list(reto_meses_dict.keys())
    
    return render(request,"challenges/index.html",{
        "meses":meses
         
    })

def meses_a_numero(request, meses):
    meses2 = list(reto_meses_dict.keys())
    if meses > len(meses2):
        return HttpResponseNotFound("<h1>EL MES INGRESADO ES INVALIDO<h1>")
    redirigir_meses = meses2[meses-1]
    redirigir_path = reverse("meses-Diario",args=[redirigir_meses]) # /Diario/Enero
    return HttpResponseRedirect(redirigir_path)




def retos_meses(request, meses):
    try:
        mensaje_retos = reto_meses_dict[meses]
        

        return render(request,"challenges/challenge.html",{
            "Meta": mensaje_retos,
             "Mes":   meses    })
        
    except:
        raise Http404()
